# Advanced CSS and SASS

Contains code and notes from studying [Advanced CSS and SASS](https://www.udemy.com/advanced-css-and-sass/learn/v4/overview)

## Intro

Materials are hosted here: [Git Hub Page](https://github.com/jonasschmedtmann/advanced-css-course)

Jonas resources page is here: [Jonas resources page](http://codingheroes.io/resources/)

Jonas twitter account is here: [@jonasschmedtman](https://twitter.com/jonasschmedtman)

Jonas Facebook page is here: [Jonas Facebook page](https://www.facebook.com/codingheroes/)

## Section 2: Natours: Setup and first steps

### Building the Header: Part 1

This section contains:

- How to perform basic reset using the universal selector
- How to set-up project-wide font definitions
- How to clip parts of element using clip-path

Links:

[Clippy: clip-path tool](https://bennettfeely.com/clippy/)

### Building the Header: Part 2

We are adding the logo image inside the header. We wrap it in a `div.logo-box`.

#### How to: Absolute positioning

Position `absolute` - the measurement start from where? It starts from that _parent_, where it's position is `relative`:

```css
.header {
  position: relative;
}
.logo-box {
  position: absolute;
  top: 40px;
  left: 40px;
}
```

, and the element:

```html
<header class="header">
    <div class="logo-box">
        <img src="img/logo-white.png" alt="Logo" class="logo"/>
    </div>
</header>
```

#### How to: Resize image proportionally

Controlling _image size_: set the `height`, the width `will` will scale proportionally:

```css
.logo {
  height: 35px; /* width will scale accordingly */
}
```

Next we are adding the heading text.

#### How to: Make span occupy the complete width

To make a `span` to occupy the complete width, we set `display: block;`.

#### How to: Set font-weight accordingly to font weight

When setting `font-weight`, we should match the font weight:

```html
<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
```

, and we set style: `font-weight: 700;`

#### How to: Center element inside parent

Center `div.text-box` onto the parent `header.header`, we need to use the css property `translate`:

```css
.text-box {
  position: absolute;
  top: 40%;
  left: 50%;
  transform: translate(-50%, -50%)
}
```

Remember: the parent has to have `position: relative;`.

### Building the Header: CSS Animations

Learning how to create CSS animations using `@keyframes` and the `animation` property.

Two types of animation in CSS:

1. Use the `transition` property, then change the properties that we want to animate on an event, like _hover_.
1. Specify the steps of the animation using the `@keyframes` and the `animation` property.

Next we are using the second method.

The browsers are optimized for 2 properties to animate: `opacity` and `transform`.

Tip: Sometimes, the animated elements have some _side effect_ - will _shake vertically_ a little. In order to prevent this, we add style `backface-visibility: hidden;` to the parent container, as what it is - _the entire parent element_ actually shakes vertically.

```css
.heading-primary {
  backface-visibility: hidden;
}
```

#### How to: Select same words in the document

Having selected a word, press [Ctrl]+[D] to select next same word. Select more words with pressing multiple times the combination.

#### How to: Start animation on hover instead of on load

Use CSS `:hover` selector:

```css
.logo:hover {
    animation: moveInRight 1s ease-out;
}
```

### Building Complex Animated Button

What we will learn:

- What `pseudo-elements` and `pseudo-classes` are
- How and why to use the `::after` pseudo-element
- How to create a creative hover animation effect using the `transition` property

**Pseudo classes** are _special state_ on the selector.

#### How to: Pseudo class a link

Having an html link:

```html
<a href="#" class="btn btn-white">Discover our tours</a>
```

, we want to style both normal (never clicked before) and visited (clicked once) presentation:

```css
.btn:link
.btn:visited {

}
```

#### How to: Center inline elements

Inline elements are just text:

```html
            <div class="text-box">
                <h1 class="heading-primary">
                    <span class="heading-primary-main">Outdoors</span>
                    <span class="heading-primary-sub">is where life happens</span>
                </h1>
                <a href="#" class="btn btn-white">Discover our tours</a>
            </div>
```

In order to center the above two headings as well as the html link button:

```css
.text-box {
  text-align: center;
}
```

#### How to: Make button move up on hover

To make the button to move up, we transform direction Y:

```css
.btn:hover {
  transform: translateY(-3px);
}
```

#### How to: Set all `transition` based animation to have same time

Add `transition` property _on the initial state_. Use `all` as type:

```css
.btn:link,
.btn:visited {
  transition: all .2s;
}
```

#### How to: Add saddle shadow on hover

Use:

```css
box-shadow: 0 10px 20px rgba(0, 0, 0, .2);
```

, where the parameters are:

- offset Y
- offset X
- blur radius
- color

### Building Complex Animated Button, Part 2

We are adding a `.btn::after` pseudo element.

#### How to: Add a child pseudo element of same size

We are adding a pseudo element that is exactly the same (same dimensions, ) as the original element and has `z-index: -1;`

```css
.btn:link,
.btn:visited {
...
  position: relative;
}
...
.btn::after {
  content: "";
  display: inline-block;
  height: 100%;
  width: 100%;
  border-radius: 100px;
  position: absolute;
  top: 0;
  left: 0;
  z-index: -1;
  transition: all .4s;
}

.btn-white::after {
  background-color: #fff;
}

.btn:hover::after {
  transform: scaleX(1.4) scaleY(1.6);
  opacity: 0;
}

```

Remember: the `position: absolute;` needs a reference, and this reference is the first parent element with `position: relative;`.

In our example, we need to set the iniitial `btn:link,.btn:visited` to `position: relative`.

#### How to: Start animation after .2 sec

What the next does is:

- Start with delay of .2s
- Automatically apply the styles at 0% before animation starts.

```css
.btn-animated {
  animation: moveInBottom 0.3s ease-out .2s;
  animation-fill-mode: backwards;
}
```

## Section 3: How CCC Works

### Three Pillars of writing good HTML and CSS

The three pillars of writing good HTML and CSS are:

- Responsive design
- Maintainable and scalable code
- Web performance

Responsive design:

- Fluid layouts
- Media queries
- Responsive images
- Correct units (font sizes, dimensions)
- Desktop-first vs mobile-first

Maintainable and scalable code:

- Clean code
- Easy-to-understand
- Growth
- Reusable
- How to organize files
- How to name classes
- How to structure HTML

Web performance:

- Less HTTP requests
- Less code
- Compress code
- Use a CSS preprocessor
- Less images
- Compress images

![Three pillars writing of good HTML and CSS](./doc/img/three-pillars-of-good-html-and-css-01.png)

### Loading HTML and CSS in the browser

What happens in the browser when loading the HTML:

![Browser loading HTML and CSS](./doc/img/browser-loading-html-and-css-01.png)

### The Cascade and Specificity

This is about the **CSS parsing phase**.

A **CSS Rule** has the following parts:

- selector
- declaration block
- declaration
- property
- declared value

![CSS Rule](./doc/img/css-rule-01.png)

The CSS parsing phase has two parts:

- Parse conflicting  CSS declarations (cascade)
- Process final CSS values

**Cascade** is:

    The process of combining different stylesheets
    and resolving conflicts between different CSS rules and declarations,
    when more than one rule applies to a certain element.

The source of declarations is:

- Author
- User
- Browser (user agent)

The cascade applies in the following order:

- Importance (weight)
- Specificity
- Source order

, in order to determine which ones take precedence

#### Importance

The _importance_ is evaluated in the following order:

- User `!important` declaration
- Author `!important` declaration
- Author declarations
- User declarations
- Default browser declarations

Keyword `!important` - changes the precedence, as in the next example:

![Keyword !important example](./doc/img/keyword-important-example-01.png)

, the _author !important_ will take precedence even over the more specific declaration.

#### Specificity

When we have conflicting declarations after importance is evaluated, _specificity_ evaluates in the following order:

- Inline styles
- IDs
- Classes, pseudo-classes, attribute
- Elements, pseudo-elements

All these declarations have the same importance as they are author declarations. Based on the specificity, these selectors:

![Keyword !important example](./doc/img/specificity-example-01.png)

, are calculated by specificity:

![Keyword !important resolution](./doc/img/specificity-example-resolution-01.png)

The _value of the wining declaration_ is called **cascaded value**, because is a result from a cascade.

#### Source order

If the specificity of two selectors is the same, then the _last one in the code wins_.

![Importance, Specificity, Source Order](./doc/img/importace-specificity-source_order-01.png)

The **universal selector \*** has no specificity:

  \* specificity is: (0, 0, 0, 0)

Tip:

  Always rely on the specificity, not on the order of selector.

, so you can easy refactor the CSS code.

Tip:

  Always put your author stylesheets last, so you rely on order of the code for cascade.

#### Codepen.io example

See example "CSS: Cascade and Specificity" at [Codepen.io](https://codepen.io/atanashristov/pen/VypVqZ)

### Value Processing

The _declared values_ are processed to the final _actual value_ in 6 different steps:

- Declared value: (author declarations)
- Cascaded value: (after the cascade)
- Specified value: (defaulting if there is no cascaded value)
- Computed value: (converting relative values to absolute: rem to px; keywords - orange, bold, etc.)
- Used value: (final calculations based on layout)
- Actual value: (browser and device restrictions)

Having the following HTML:

```html
<div class="section">
  <p class="amazing">CSS is absolutely amazing</p>
</div>
```

, and the CSS:

```css
.section {
  font-size: 1.5em;
  width: 280px;
  background-color: orangered;
}
p {
  width: 140px;
  background-color: green;
}
.amazing {
  width: 66%;
}
```

, we get something similar to:

![CSS processing example - result](./doc/img/css-processing-values-example-result-01.png)

See example "CSS: Processing Values" at [Codepen.io](https://codepen.io/atanashristov/pen/xpqQNe)

Value processing `width`:

- Conflicting declared values: Cascade applied the specificity
- Final conversion percentage to pixels: Relative to the parent width

![Value processing width](./doc/img/value-processing-example-width-01.png)

Value processing `padding`:

- Every CSS property has _initial value_
- When there is no _cascaded value_, then the _initial value_ is used
- `padding` property has initial value of `0px`
- Nothing to compute further - the initial value is in pixels

![Value processing padding](./doc/img/value-processing-example-padding-01.png)

Value processing `font-size` (root):

- There is no author declared value
- Cascaded value comes from source _browser default_

![Value processing font-size root](./doc/img/value-processing-example-fontsize-root-01.png)

Value processing `font-size` (section):

- Declared value is in relative units `rem`
- The engine converts into pixels
- The computed value calculated relative to the root size

![Value processing font-size section](./doc/img/value-processing-example-fontsize-section-01.png)

Value processing `font-size` (paragraph):

- There is no author declaration
- It _inherits_ the computed value from the parent

**IMPORTANT:** _Inheritance_ - some properties, such as related to text, as `font-size`, inherit the computed value from their parents

![Value processing font-size paragraph](./doc/img/value-processing-example-fontsize-paragraph-01.png)

#### Converting relative to absolute (px) units

Font-based relative units:

- `rem`: Use the root font size as a reference
- `em` (font): Use the parent element's font size as a reference
- `em` (length): Uses the computed current element font size

Tip: it is a great technique to base lengths on font size for responsive layouts.

Viewport-based relative units:

- `vh`: 1vh is 1% of the view point height
- `vw`: 1vw is 1% of the view point width

![Convert relative to absolute units](./doc/img/convert-relative-to-absolute-units-01.png)

#### Summary

![Value processing summary](./doc/img/value-processing-summary-01.png)

### Inheritance

What is inheritance:

  A way to propagating values from parents to their children

In the following example, the `line-height` of the `.child` will be based on the **computed value** of the `.parent`: _30px;_:

![Inheritance](./doc/img/inheritance-01.png)

What we need to know about inheritance:

![Inheritance summary](./doc/img/inheritance-summary-01.png)

### Converting px to rem - an Effective Flow

We use a 2 step process.

**Step 1:** Set the root `font-size` property of the `html` selector to `10px`.

Like this:

```css
html {
  font-size: 10px;
}
```

Now we know that `1rem` is _exactly 10px_! Because `1rem` is exactly _the root font size_.

**Step 2:** Convert the root `font-size` property of the `html` selector to percentage of the default browser font size (16px usually).

After we replace all the `px` units with `rem`, we come back to and convert the `font-size` attribute of the `html` to percentage based value.

Having 16px as the default browser font size, so we calculate:

  10 / 16 = 0.625

```css
html {
  font-size: 62.5%;
}
```

### Example of Inheritance

We have the following reset:

```css
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
```

If we can use inheritance, we should do that:

```css
*,
*::after,
*::before {
  margin: 0;
  padding: 0;
  box-sizing: inherit;
}

body {
  box-sizing: border-box;
}
```

Also, we include the `*::before` and `*::after` pseudo elements.

### The Visual Formatting Model

Definition:

    The Visual formatting model is an algorithm that calculates boxes and determines the layout of these boxes, for each element in the render tree, in order to determine the final layout of the page.

This includes:

- **Dimensions of boxes:** the box model
- **Box type:** inline, block and inline-block
- **Positioning scheme:** floats and positioning;
- **Stacking contexts**
- Other elements in the render tree
- Viewpoint size, dimensions of images, etc.

#### The Box Model

The margin is a space between the boxes (assuming we have `box-sizing: border-box;`):

![The box model](./doc/img/the-box-model-01.png)

Box sizing `box-sizing: border-box;`:

![Box Sizing Border Box](./doc/img/box-sizing-border-box-01.png)

#### Box Model Types

The box types are:

- Block Level
  - div, p are block-level by default
  - always occupy as much space as possible
  - create line breaks before and after
- Inline
- Inline-block

Codepen.io example: [CSS: Box types](https://codepen.io/atanashristov/pen/BJRgRB)

![Box types](./doc/img/box-types-01.png)

#### Positioning

They are three types of positioning in CSS:

- Normal flow
- Floats
  - shift left or right as far as possible
  - to adjust container height - use clear-fixes
- Absolute positioning
  - absolute
    - the element has no impact on surrounding content or elements
    - will overlap in fact
  - fixed

![Positioning Schemes](./doc/img/positioning-schemes-01.png)

### CSS Architecture, Components and BEM

#### Naming classes

**"BEM"** naming convention:

  **B**lock **E**lement **M**odifier

, where:

- Block: standalone component that is meaningful on its own
- Element: part of a block, has no meaning on it's own
- Modifier: a flag on the element to make it different from the other same elements

![BEM](./doc/img/block-element-modifier-01.png)

Tip: It uses low-specificity - we always use _classes_ and they are _never nested_.

#### Folder and file structure

**7-1 pattern** for folders and files:

![7-1 pattern](./doc/img/7-1-pattern-01.png)

, where:

- base: basic project definitions
- components: 1 file for each component
- layout: define the layout of the project
- pages: styles for specific pages
- themes: if we want to implement themes
- abstract: code that does not output any CSS - variables, mixins
- vendors: 3rd party CSS

Note: Sass - can be any preprocessor.

